package ru.konovalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.api.other.ISaltSettings;

public interface IPropertyService extends ISaltSettings {

    @NotNull String getApplicationVersion();

    int getBackupInterval();
}
