package ru.konovalov.tm.comparator;

import ru.konovalov.tm.api.entity.IHasDateStart;

import java.util.Comparator;

public class ComparatorByDateStart implements Comparator<IHasDateStart> {

    private final static ComparatorByDateStart INSTANCE = new ComparatorByDateStart();

    private ComparatorByDateStart() {
    }

    public static ComparatorByDateStart getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(IHasDateStart o1, IHasDateStart o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getDateStart().compareTo(o2.getDateStart());
    }

}
