package ru.konovalov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.api.other.ISaltSettings;

import java.security.NoSuchAlgorithmException;

public interface HashUtil {

    @NotNull
    String SECRET = "666777999";
    @NotNull
    Integer ITERATION = 20000;

    static String salt(
            @Nullable final ISaltSettings settings,
            @Nullable final String value
    ) {
        if (settings == null) return null;
        @Nullable final String secret = settings.getPasswordSecret();
        @Nullable final Integer iteration = settings.getPasswordIteration();
        return salt(value, iteration, secret);
    }

    static String salt(
            @Nullable final String value,
            @NotNull Integer iteration,
            @NotNull String secret) {
        if (value == null) return null;
        @Nullable String result = value;
        for (int i = 0; i < iteration; i++) {
            result = md5(secret + result + secret);
        }
        return result;
    }

    static String md5(String value) {
        if (value == null) return null;
        try {
            java.security.MessageDigest md = java.security.MessageDigest.getInstance("MD5");
            final byte[] array = md.digest(value.getBytes());
            final StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; ++i) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1, 3));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }

}
