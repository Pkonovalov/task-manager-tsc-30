package ru.konovalov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.api.repository.ICommandRepository;
import ru.konovalov.tm.command.AbstractCommand;


import java.util.*;

import static ru.konovalov.tm.util.ValidationUtil.isEmpty;

public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> arguments = new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();

    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return commands.values();
    }

    @NotNull
    public Collection<AbstractCommand> getArguments() {
        return arguments.values();
    }

    @NotNull
    @Override
    public Collection<String> getCommandNames() {
        @NotNull final List<String> result = new ArrayList<>();
        commands.values().stream()
                .filter(e -> !isEmpty(e.name()))
                .forEach(e -> result.add(e.name()));
        return result;
    }

    @NotNull
    @Override
    public Collection<String> getCommandArgs() {
        @NotNull final List<String> result = new ArrayList<>();
        commands.values().stream()
                .filter(e -> !isEmpty(e.arg()))
                .forEach(e -> result.add(e.arg()));
        return result;
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@NotNull String name) {
        return commands.get(name);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByArg(@NotNull String name) {
        return arguments.get(name);
    }

    @Override
    public void add(@NotNull AbstractCommand command) {
        @Nullable final String arg = command.arg();
        @NotNull final String name = command.name();
        if (arg != null) arguments.put(arg, command);
        if (name != null) commands.put(name, command);
    }

}
