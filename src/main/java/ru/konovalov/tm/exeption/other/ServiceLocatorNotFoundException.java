package ru.konovalov.tm.exeption.other;

import org.jetbrains.annotations.NotNull;
import ru.konovalov.tm.exeption.AbstractException;

public class ServiceLocatorNotFoundException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Service locator not found!";

    public ServiceLocatorNotFoundException() {
        super(MESSAGE);
    }

}
