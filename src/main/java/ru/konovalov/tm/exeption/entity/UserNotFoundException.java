package ru.konovalov.tm.exeption.entity;

public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException() {
        super("Error! User not found...");
    }

}
