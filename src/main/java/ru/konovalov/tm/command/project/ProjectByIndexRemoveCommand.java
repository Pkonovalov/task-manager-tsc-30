package ru.konovalov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.util.TerminalUtil;

public class ProjectByIndexRemoveCommand extends AbstractProjectCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-remove-by-index";
    }

    @Override
    public @NotNull String description() {
        return "Remove project by index";
    }

    @Override
    public void execute() {
        final @NotNull String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[REMOVE PROJECT]");
        System.out.println("ENTER INDEX:");
        serviceLocator.getProjectService().removeProjectByIndex(userId, TerminalUtil.nextNumber());
    }

}
