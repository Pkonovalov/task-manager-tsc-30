package ru.konovalov.tm.command.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.dto.Domain;
import ru.konovalov.tm.enumerated.Role;

import java.io.FileOutputStream;

public final class BackupSaveCommand extends AbstractDomainCommand {

    @NotNull
    public final static String NAME = "backup-save";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Nullable
    @Override
    public String description() {
        return "Save backup to XML";
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

    @Override
    public @Nullable Role[] roles() {
        return null;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(BACKUP_XML);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

}
