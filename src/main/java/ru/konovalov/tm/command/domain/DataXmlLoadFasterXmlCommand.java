package ru.konovalov.tm.command.domain;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.dto.Domain;

import java.nio.file.Files;
import java.nio.file.Paths;

public final class DataXmlLoadFasterXmlCommand extends AbstractDomainCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Nullable
    @Override
    public String description() {
        return "Load xml data from file";
    }

    @NotNull
    @Override
    public String name() {
        return "data-xml-load";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA XML LOAD]");
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(FILE_FASTERXML_XML)));
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

}
