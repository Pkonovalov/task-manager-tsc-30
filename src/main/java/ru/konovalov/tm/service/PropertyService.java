package ru.konovalov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private static final String APPLICATION_VERSION_DEFAULT = "";
    @NotNull
    private static final String APPLICATION_VERSION_KEY = "application.version";
    @NotNull
    private static final String BACKUP_INTERVAL_DEFAULT = "15000";
    @NotNull
    private static final String BACKUP_INTERVAL_KEY = "backup.interval";
    @NotNull
    private static final String FILE_NAME = "application.properties";
    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "1";
    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";
    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "";
    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";
    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        @Nullable final String systemProperty = System.getProperty(APPLICATION_VERSION_KEY);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getenv(APPLICATION_VERSION_KEY);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(APPLICATION_VERSION_KEY, APPLICATION_VERSION_DEFAULT);
    }

    @Override
    public int getBackupInterval() {
        @Nullable final String systemProperty = System.getProperty(BACKUP_INTERVAL_KEY);
        if (systemProperty != null) return Math.max(Integer.parseInt(systemProperty), 5000);
        @Nullable final String environmentProperty = System.getenv(BACKUP_INTERVAL_KEY);
        if (environmentProperty != null) return Math.max(Integer.parseInt(environmentProperty), 5000);
        final int interval = Integer.parseInt(properties.getProperty(BACKUP_INTERVAL_KEY, BACKUP_INTERVAL_DEFAULT));
        return Math.max(interval, 5000);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        @Nullable final String systemProperty = System.getProperty(PASSWORD_SECRET_KEY);
        if (systemProperty != null) return Integer.parseInt(systemProperty);
        @Nullable final String environmentProperty = System.getenv(PASSWORD_SECRET_KEY);
        if (environmentProperty != null) return Integer.parseInt(environmentProperty);
        return Integer.parseInt(
                properties.getProperty(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT)
        );
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        @Nullable final String systemProperty = System.getProperty(PASSWORD_SECRET_KEY);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getenv(PASSWORD_SECRET_KEY);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

}