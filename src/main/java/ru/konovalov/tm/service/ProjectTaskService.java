package ru.konovalov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.konovalov.tm.api.repository.IProjectRepository;
import ru.konovalov.tm.api.service.IProjectTaskService;
import ru.konovalov.tm.api.repository.ITaskRepository;
import ru.konovalov.tm.exeption.empty.EmptyIdException;
import ru.konovalov.tm.exeption.empty.EmptyNameException;
import ru.konovalov.tm.exeption.entity.ProjectNotFoundException;
import ru.konovalov.tm.model.Project;
import ru.konovalov.tm.model.Task;

import java.util.List;

import static ru.konovalov.tm.util.ValidationUtil.isEmpty;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    public ProjectTaskService(@NotNull final IProjectRepository projectRepository, ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Nullable
    @Override
    public List<Task> findALLTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        return taskRepository.findALLTaskByProjectId(userId, projectId);
    }

    @Nullable
    @Override
    public Task assignTaskByProjectId(@NotNull final String userId, @NotNull final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException("Task");
        return taskRepository.assignTaskByProjectId(userId, projectId, taskId);
    }

    @Nullable
    @Override
    public Task unassignTaskByProjectId(@NotNull final String userId, @Nullable final String taskId) {
        if (taskId == null || taskId.isEmpty()) throw new EmptyIdException("Task");
        return taskRepository.unassignTaskByProjectId(userId, taskId);
    }

    @Nullable
    @Override
    public List<Task> removeTasksByProjectId(@NotNull final String userId, @Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new EmptyIdException("Project");
        return taskRepository.removeAllTaskByProjectId(userId, projectId);
    }

    @Nullable
    @Override
    public Project removeProjectById(@NotNull final String userId, @NotNull final String projectId) {
        if (isEmpty(projectId)) throw new EmptyIdException();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        projectRepository.removeById(userId, projectId);
        return null;
    }

    @Override
    public void removeProjectByName(@NotNull final String userId, @Nullable final String projectName) {
        if (isEmpty(projectName)) throw new EmptyNameException();
        @NotNull String projectId = projectRepository.getIdByName(userId, projectName);
        if (isEmpty(projectId)) throw new EmptyIdException();
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        projectRepository.removeOneByName(userId, projectName);
    }


}
